package ua.dp.krotova.hw11.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class CommonWebDriver {

    protected WebDriver driver;

    @BeforeSuite
    public void beforeClass() {
        driver = new FirefoxDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

}
