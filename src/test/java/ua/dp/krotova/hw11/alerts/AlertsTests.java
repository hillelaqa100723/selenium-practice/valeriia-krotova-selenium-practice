package ua.dp.krotova.hw11.alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.dp.krotova.hw11.common.CommonWebDriver;

public class AlertsTests extends CommonWebDriver {

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }

    @Test
    public void jsConfirmOkTest() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsConfirmCancelTest() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String textToEnter = "Prompt text!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, textToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + textToEnter, resultText);
    }

    @Test
    public void jsPromptEmptyTest() {
        String expectedTextAtAlert = "I am a JS prompt";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:", resultText);
    }

    @Test
    public void jsPromptCancelTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String textToEnter = "Prompt text!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false, textToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);
    }

    @Test
    public void jsPromptEmptyCancelTest() {
        String expectedTextAtAlert = "I am a JS prompt";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);
    }

    @Test
    public void jsConfirmOkJsTest() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";

        clickJSButtonWithJs(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsConfirmCancelJsTest() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";

        clickJSButtonWithJs(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsPromptJsTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String textToEnter = "Prompt text!";

        clickJSButtonWithJs(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, textToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + textToEnter, resultText);
    }

    @Test
    public void jsPromptEmptyJsTest() {
        String expectedTextAtAlert = "I am a JS prompt";

        clickJSButtonWithJs(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:", resultText);
    }

    @Test
    public void jsPromptCancelJsTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String textToEnter = "Prompt text!";

        clickJSButtonWithJs(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false, textToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);
    }

    @Test
    public void jsPromptEmptyCancelJsTest() {
        String expectedTextAtAlert = "I am a JS prompt";

        clickJSButtonWithJs(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);
    }

    private void clickJSButton(Buttons buttons) {
        findButton(buttons).click();
    }

    private void clickJSButtonWithJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement button = findButton(buttons);
        executor.executeScript("return arguments[0].click();", button);
    }

    private WebElement findButton(Buttons button) {
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }

    private String getResultText() {
        return driver.findElement(By.id("result")).getText();
    }

    private String workWithAlert(boolean accept, String... textToInput) {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();

        if (textToInput.length > 0) {
            alert.sendKeys(textToInput[0]);
        }

        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }

    enum Buttons {
        JS_ALERT("Click for JS Alert", "jsAlert()"),
        JS_CONFIRM("Click for JS Confirm", "jsConfirm()"),
        JS_PROMPT("Click for JS Prompt", "jsPrompt()");

        private final String textValue;
        private final String code;

        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;
        }

        public String getTextValue() {
            return textValue;
        }

        public String getCode() {
            return code;
        }
    }
}

